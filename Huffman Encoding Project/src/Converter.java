import java.util.Map;

public interface Converter<T,E> {
	public Map<T,E> convert();
}
