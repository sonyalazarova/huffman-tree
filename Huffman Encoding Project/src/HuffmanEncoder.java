import java.util.Map;
import java.util.Map.Entry;

public class HuffmanEncoder implements Encoder<String>{
	private int treeHeight;
	private int encodedMessageLenght;
	private HuffmanTree huffmanTree;

	public HuffmanEncoder() {
		treeHeight = 0;
		encodedMessageLenght = 0;		
		huffmanTree = new HuffmanTree();
	}

	public int getTreeHeight() {
		return treeHeight;
	}

	public int getEncodedMessageLenght() {
		return encodedMessageLenght;
	}

	@Override
	public void encode(String stringToEncode) {
		Map<Character, Integer> elements = new StringConverter(stringToEncode).convert();
		huffmanTree.create(elements);
		Map<Character, String> codeTable = huffmanTree.createCodeTable();

		for (Entry<Character, Integer> element : elements.entrySet()) {
			int codeLenght = codeTable.get(element.getKey()).length();
			int frequency = element.getValue();

			encodedMessageLenght += codeLenght * frequency;
		}

		treeHeight = findLongestElementCode(codeTable) + 1;
	}

	private int findLongestElementCode(Map<Character, String> codeTable) {
		int longestCode = 0;

		for (Entry<Character, String> codeElement : codeTable.entrySet()) {
			if (longestCode < codeElement.getValue().length()) {
				longestCode = codeElement.getValue().length();
			}
		}

		return longestCode;
	}

}
