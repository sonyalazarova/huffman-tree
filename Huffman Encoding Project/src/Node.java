
public class Node implements Comparable<Node> {
	private char data;
	private int frequency;
	private Node left;
	private Node right;

	public Node(char data, int frequency, Node left, Node right) {
		this.data = data;
		this.frequency = frequency;
		this.left = left;
		this.right = right;
	}

	@Override
	public int compareTo(Node node) {
		if (frequency > node.frequency) {
			return 1;
		}
		if (frequency < node.frequency) {
			return -1;
		}
		return Integer.compare(data, node.data);
	}

	public char getData() {
		return data;
	}

	public int getFrequency() {
		return frequency;
	}

	public Node getLeft() {
		return left;
	}

	public Node getRight() {
		return right;
	}

	public boolean isLast() {
		return left == null && right == null;
	}

}
