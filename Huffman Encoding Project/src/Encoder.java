
public interface Encoder<T> {
	public void encode(T messageToEncode);
}
