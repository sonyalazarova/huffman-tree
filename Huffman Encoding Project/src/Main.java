
public class Main {

	public static void main(String[] args) {
		HuffmanEncoder huffman = new HuffmanEncoder();
		String message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		huffman.encode(message);

		System.out.println("Message : " + message);
		System.out.println("\n Message length is : " + huffman.getEncodedMessageLenght());
		System.out.println("\n Tree height is : " + huffman.getTreeHeight());
	}

}
