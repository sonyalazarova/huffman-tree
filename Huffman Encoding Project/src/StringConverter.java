import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StringConverter implements Converter<Character, Integer> {
	private String stringToConvert;

	public StringConverter(String stringToConvert) {
		this.stringToConvert = stringToConvert;
	}

	@Override
	public Map<Character, Integer> convert() {
		HashMap<Character, Integer> frequencyMap = new HashMap<>();		
		char[] symbols = stringToConvert.toCharArray();
		boolean visitedSymbols[] = new boolean[symbols.length];		

		Arrays.fill(visitedSymbols, false);

		for (int i = 0; i < symbols.length; i++) {
			int frequencyCount = 1;

			if (visitedSymbols[i]) {
				continue;
			}

			for (int j = i + 1; j < symbols.length; j++) {
				if (symbols[i] == symbols[j]) {
					frequencyCount++;
					visitedSymbols[j] = true;
				}
			}

			frequencyMap.put(symbols[i], frequencyCount);
		}

		return frequencyMap;
	}
}
