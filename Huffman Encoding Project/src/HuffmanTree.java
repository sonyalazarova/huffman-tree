import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Map.Entry;

public class HuffmanTree {

	private PriorityQueue<Node> tree;
	
	public HuffmanTree() {
		this.tree = new PriorityQueue<>();
	}

	public void create(Map<Character, Integer> huffmanElements) {
		for(Entry<Character, Integer> element : huffmanElements.entrySet()) {
			tree.add(new Node(element.getKey(), element.getValue(), null, null));
		}
		
		while(tree.size() > 1) {
			Node left = tree.poll();
			Node right = tree.poll();
			
			tree.add(new Node('-', left.getFrequency() + right.getFrequency(), left, right));
		}
	}

	public Map<Character, String> createCodeTable() {
		HashMap<Character, String> codeTable = new HashMap<>();
		String elementCode = "";
		
		insertElement(codeTable, tree.element(), elementCode);
		
		return codeTable;
	}

	private void insertElement(Map<Character, String> codeTable, Node node, String elementCode) {
		
		if(!node.isLast()) {
			insertElement(codeTable, node.getLeft(), elementCode + "0");
			insertElement(codeTable, node.getRight(), elementCode + "1");
		}
		
		codeTable.put(node.getData(), elementCode);
	}
}
